# Nextcloud server ansible playbook

The playbook will set up nextcloud server on a clean installation of Almalinux 9.

It is using the following stack:

- Almalinux 9 - Latest stable release
- PostgreSQL 13 - From the official almalinux repository
- Nginx 1.20 - From the official almalinux repository
- Redis server 6.2.6 - From the official almalinux repository
- PHP 8.0.13 - From the official almalinux repository
- Nextcloud 24 - Latest stable release

## Parameters

You can use your own values for the following varables:

- db_name: The database name
- db_user: The username used for database connections
- db_pass: The password for database user
- my_domain: The domain name of your nextcloud setup
- redis_password: The password for redis connections
- nextcloud_port: The http port of your nextcloud setup

## Notes

- After successful playbook run, you need to go to http://<your_domain>:<your_port>/nextcloud/ and complete the configuration.
- You need also to edit the config.php file like the next_example_config.php

## Related articles

https://www.epilis.gr/en/blog/2022/06/23/nextcloud-delivered-ansible/
