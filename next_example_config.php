<?php
$CONFIG = array (
  'instanceid' => 'ocsvd69hotkm',
  'passwordsalt' => '1GXTiLi6WtbM3IEazRpYdEbx6QnlK8',
  'secret' => 'ULgDlvoujo9GUL3y3qEJAvVHTPV3Eya27OU4EnF1djbOnEae',
  'trusted_domains' =>
    array (
      0 => 'next.local:8080',
    ),
  'datadirectory' => '/usr/share/nginx/html/nextcloud/data',
  'dbtype' => 'pgsql',
  'version' => '24.0.1.1',
  'overwrite.cli.url' => 'http://next9.local:8080/nextcloud',
  'dbname' => 'nextclouddb',
  'dbhost' => 'localhost',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'dbuser' => 'dbuser',
  'dbpassword' => 'dbpass',
  'memcache.local' => '\OC\Memcache\Redis',
  'memcache.distributed' => '\OC\Memcache\Redis',
  'memcache.locking' => '\OC\Memcache\Redis',
  'redis' =>
    array (
     'host'     => '127.0.0.1',
     'port'     => 6379,
     'dbindex'  => 0,
     'password' => 'foobared',
     'timeout'  => 1.5,
    ),
  'installed' => true,
  'default_phone_region' => 'GR',
);
